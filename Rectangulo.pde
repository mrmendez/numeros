PFont p;

public class Rectangulo{
   float x;
   float y;
   float ancho;
   float alto;
   float vel;
   int numero=0;
    boolean ban=true;   
   public Rectangulo(float a, float b, float an, float al){
     x = a;
     y = b;
     ancho = an;
     alto = al;
     vel = (float)(Math.random()*3 + 1);
     p = createFont("ARIAL",15);
     numero = (int)(Math.random()*10+1);
   }
   
   public void dibujar(){
       fill(255,0,0);
       rect(x,y,ancho,alto);
       fill(255);
       text(""+numero, x+(ancho/2)-4 , y+(alto/2)+3);
   }

   public void mover(){
     if(ban){
      this.x = x + vel;
     }
      if(x > width){
         x =0;
         numero = (int)(Math.random()*10+1);
      }
   }

   public boolean detener(float a, float b){
         if (a >= x && a <= (x+ancho) && b>=y  && b <= (y+alto)  ){
               ban = !ban;  
               return true;          
         }
         return false;
   }

}
